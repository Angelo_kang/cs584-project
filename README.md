# Latent Semantic Indexing using Streaming Matrix Sketch


### Algorithms

1. Frequency-Direction Algorithm
	* input: l, A(term-document matrix)
	* output: matrix
2. Latent Semantic Indexing(LSI)
	* input: matrix
	* not decided yet
3. Word Counting
4. User query information retrieval


### Dataset

[Reuters-21578 Text Categorization Collection](http://kdd.ics.uci.edu/databases/reuters21578/reuters21578.html)

